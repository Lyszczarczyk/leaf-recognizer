from typing import List
import numpy
from io import BytesIO

from PIL import Image
import tensorflow


def read_image(encoded_image) -> Image.Image:
    """
    Reads encoded image and returns decoded one
    :param encoded_image:encoded image
    :return: decoded image
    """
    decoded_image = Image.open(BytesIO(encoded_image))
    return decoded_image


def recognize(image: Image.Image, model_path: str, labels: List[str]) -> List[dict]:
    interpreter = tensorflow.lite.Interpreter(model_path=model_path)
    interpreter.allocate_tensors()
    input_details = interpreter.get_input_details()
    output_details = interpreter.get_output_details()

    floating_model = input_details[0]['dtype'] == numpy.float32
    height = input_details[0]['shape'][1]
    width = input_details[0]['shape'][2]

    resized_img = image.resize((width, height))
    input_data = numpy.expand_dims(resized_img, axis=0)
    input_mean = 0  # default mean
    input_std = 255  # default std

    if floating_model:
        input_data = (numpy.float32(input_data) - input_mean) / input_std
    interpreter.set_tensor(input_details[0]['index'], input_data)
    interpreter.invoke()

    output_data = interpreter.get_tensor(output_details[0]['index'])
    results = numpy.squeeze(output_data)
    top_three_results = results.argsort()[-3:][::-1]
    predictions = list()
    for i in top_three_results:
        if floating_model:
            predictions.append(dict(name=labels[i], confidence=float(results[i])))
        else:
            predictions.append(dict(name=labels[i], confidence=float(results[i]/255.0)))
    return predictions






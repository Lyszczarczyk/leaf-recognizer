import os
from typing import List

LABELS_PATH = os.path.join(os.path.dirname(__file__), 'class_labels.txt')
MODEL_PATH = os.path.join(os.path.dirname(__file__), 'new_mobile_model.tflite')


def load_labels(filename: str) -> List:
    with open(filename, 'r') as f:
        return [line.strip() for line in f.readlines()]


LABELS = load_labels(LABELS_PATH)

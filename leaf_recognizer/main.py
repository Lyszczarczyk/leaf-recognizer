from fastapi import FastAPI, UploadFile, File, HTTPException
from fastapi.middleware.cors import CORSMiddleware
from starlette.responses import RedirectResponse

from leaf_recognizer.assets import MODEL_PATH, LABELS
from leaf_recognizer.recognize import read_image, recognize

app = FastAPI()
app.add_middleware(CORSMiddleware,
                   allow_origins=['*'],
                   allow_methods=['*'],
                   allow_headers=['*'])


@app.get("/", include_in_schema=False)
async def root():
    return RedirectResponse(url="/docs")


@app.post("/recognize")
async def recognize_leaf(file: UploadFile = File(...)):
    extension = file.filename.split(".")[-1] in ("jpg", "jpeg", "png")
    if not extension:
        raise HTTPException(status_code=404, detail="Image must be of jpg or png format!")
    image = read_image(await file.read())
    predictions = recognize(image, MODEL_PATH, LABELS)
    return predictions
